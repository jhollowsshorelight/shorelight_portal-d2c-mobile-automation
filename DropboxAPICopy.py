import requests
from datetime import datetime
import sys
import time


arg = str(sys.argv[1])
print arg
headers = {
    'Authorization': 'Bearer WzIb6El58sAAAAAAAAAGlinoturEZqQzHNQtAPcvepL_vJS-E0UzTZTEsF2GKJUS',
    'Content-Type': 'application/json',
}

# this function Create new Folder under test results
todaysdate = datetime.now().strftime('%Y_%m_%d_%H_%M_%S')
print todaysdate
foldername = "/Automation_Test_Results/Student_Portal_Automation_Results/"

if arg == 'QA': 
    foldername = foldername + "QA-Mobile/"
elif arg == 'STAGING':
    foldername = foldername + "STAGING-Mobile/"
elif arg == 'CHINA':
    foldername = foldername + "CHINA-Mobile/"

foldername = foldername + todaysdate

print foldername
dd1 = '{"title": "Student Portal Test Result Run","destination": "filelocation","deadline": {"deadline": "2019-10-12T17:00:00Z","allow_late_uploads": "seven_days"},"open": true}'

dd1 = dd1.replace('filelocation', foldername)
data = dd1

try:
    response = requests.post('https://api.dropboxapi.com/2/file_requests/create', headers=headers, data=data)
    print response


    # this function uploada the Testresult files as GZ file to newly Created folder
    data2 = '{"path": "TestResult/results.tgz","mode": "add","autorename": true,"mute": false,"strict_conflict": false}'
    data2 = data2.replace('TestResult', foldername )

    print data2

    headers2 = {
        'Authorization': 'Bearer WzIb6El58sAAAAAAAAAGlinoturEZqQzHNQtAPcvepL_vJS-E0UzTZTEsF2GKJUS',
        'Dropbox-API-Arg': data2,
        'Content-Type': 'application/octet-stream',
    }

    data1 = open('/drone/src/bitbucket.org/shorelight/shorelight_portal-d2c-mobile-automation/results.tgz', 'rb').read()
    response1 = requests.post('https://content.dropboxapi.com/2/files/upload', headers=headers2, data=data1)

    print response1

except (RuntimeError, TypeError, NameError):
    print("Unexpected error:", sys.exc_info()[0])