*** Settings ***
Test Teardown    Quit Application
Resource          ../Mobile-Libraries-Resources-Variables.robot

*** Variables ***
${app_major}    Actuarial Science


*** Test Cases ***
Submit Undergraduate Application
    Open Phone Browser To Undergraduate Page
    Generate Faker Data
    Personal Information    ${app_major}
    Contact Information
    Education History
    Graduate Education History
    Upload Documents
    Agreements And Declarations
    #Submit Application


