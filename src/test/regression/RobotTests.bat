@ECHO OFF

SET SCRIPT_DIR=%~dp0
SET PATH=%SCRIPT_DIR%Libraries;%PATH%
SET TESTRESULTS_DIR=%SCRIPT_DIR%TestResults
ECHO SCRIPT PATH IS %SCRIPT_DIR%
ECHO TESTRESULTSPATH IS %TESTRESULTS_DIR%
set year=%date:~10,4%
set month=%date:~4,2%
set day=%date:~7,2%
set hour=%TIME:~1,1%
set minute=%TIME:~3,2%
set second=%TIME:~6,2%
set filename=%year%_%month%_%day%_%hour%_%minute%_%second%_TestResult
SET NewDIR=%TESTRESULTS_DIR%\%filename%
echo NewDIR

mkdir %NewDIR%

::CALL install.bat
::Running Robot Tests
CALL pybot --name StudentPortal_Automation %* --variable environment:staging --variable Browser:chrome --outputdir %NewDIR% --output output-gc.xml --report report-gc.html --log log-gc.html ..\TestSuite\CreateSingleStudent.robot





