import requests
import re
import logging

# Documentation:

class Utilities():
    logger = logging.getLogger('scope.name')

    def __init__(self):
        self.data = []
        file_log_handler = logging.FileHandler('logfile.log')
        self.logger.addHandler(file_log_handler)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        file_log_handler.setFormatter(formatter)

    def sendEmailValidation(self, email):
        baseUrl = 'https://d2c-qa.shorelight.ml/d2c/verify?email='
        req = requests.get(baseUrl + email)
        
        #self.logger.info('GET Request to: ' + baseUrl + email + ' RESPONSE: ' + req.text)

        return req.text

    def GetEmailCode(self, body):
        #TODO
        #May have to revisit this since we are currently matching on both parts of the email body but for now it is ok.
        regex = r'\b([0-9]{6})\b'
        code = 0
        matches = re.findall(regex, body)
        code = matches[0].strip()
        #self.logger.info('Verification Code: ' + code)

        return  code

    def GetEmailSubject(self, body):
        #TODO
        #May have to revisit this since we are currently matching on both parts of the email body but for now it is ok.
        regex = r'\b([0-9]{6})\b'
        code = 0
        matches = re.findall(regex, body)
        code = matches[0].strip()
        #self.logger.info('Verification Code: ' + code)

        return  code        