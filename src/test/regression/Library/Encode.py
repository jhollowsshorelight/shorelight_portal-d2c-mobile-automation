import base64

class Encode():

    def __init__(self):
        self.string = None

    def encoderf(self,string):
        string = base64.b64encode(string)
        return string

    def decoderf(self,string):
        string = base64.b64decode(string)
        return string