from simple_salesforce import Salesforce, SalesforceLogin, SFType
import requests
import json
import base64


class SalesForceRestApi:
    """
    This test library provides keywords to allow opening, reading, writing
     data in restapi for Salesforce from Robot Framework.


    *Before running tests*

    Prior to running tests, ExcelLibrary must first be imported into your Robot test suite.

    Example:
        | Library | SalesForceRestApi |

    """

    ROBOT_LIBRARY_SCOPE = 'GLOBAL'

    def __init__(self):
        self._sf_object = []

    def pp_json(json_thing, sort=True, indents=4):
        if type(json_thing) is str:
            print(json.dumps(json.loads(json_thing), sort_keys=sort, indent=indents))
        else:
            print(json.dumps(json_thing, sort_keys=sort, indent=indents))
        return None

    def salesforce_fields_rest(self, sfusername, sfpassword, sfsecurity_token, sf_object, sandbox=True):
        print('Opening Connection to')
        sf = Salesforce(username=sfusername, password=sfpassword, security_token=sfsecurity_token, sandbox=True)
        objj = sf_object
        objj = 'sobjects/' + str(objj) + '/describe'
        print(objj)
        fields_rest = sf.restful(path=objj, params='')
        Feilds = {}
        return json.dumps(fields_rest)

    def salesforce_recordtype_rest(self, sfusername, sfpassword, sfsecurity_token, sf_object, sandbox=True):
        print('Opening Connection to')
        sf = Salesforce(username=sfusername, password=sfpassword, security_token=sfsecurity_token, sandbox=True)
        objj = sf_object
        objj = 'sobjects/' + str(objj) + '/describe'
        # print objj
        fields_rest = sf.restful(path=objj, params='')
        rt = []
        for x in fields_rest['recordTypeInfos']:
            rt.append(str(x['name']))
            # print 'Record types:- %s'  % x['name']
        rt.remove('Master')
        print(rt)
        return rt

    def salesforce_update_rest(self, sfusername, sfpassword, sfsecurity_token, sf_objname, sf_objid, sf_object,
                               sandbox=True):
        print('Opening Connection to')
        sf = Salesforce(username=sfusername, password=sfpassword, security_token=sfsecurity_token, sandbox=True)
        sftype = getattr(sf, sf_objname)
        returned = sftype.update(sf_objid, sf_object)
        return returned

    def salesforce_create_rest(self, sfusername, sfpassword, sfsecurity_token, sf_objname, sf_object, sandobox=True):
        print('Opening Connection to')
        sf = Salesforce(username=sfusername, password=sfpassword, security_token=sfsecurity_token, sandbox=True)
        sftype = getattr(sf, sf_objname)
        returned = sftype.create(sf_object)
        return returned

    def salesforce_create_att(self, sfusername, sfpassword, sfsecurity_token, sf_parent, sf_filename, sf_descr,
                              sandbox=True):
        with open(sf_filename, "r") as f:
            body = base64.b64encode(f.read())
        sf = Salesforce(username=sfusername, password=sfpassword, security_token=sfsecurity_token, sandbox=True)
        sessionid=sf.session_id
        instance=sf.sf_instance
        '''sftype = getattr(sf, 'Attachment')
        data = json.dumps({
            'ParentId': sf_parent,
            'Name': sf_filename,
            'body': body
        })
        returned = sftype.create(data)'''
        response = requests.post('https://%s/services/data/v42.0/sobjects/Attachment/' % instance,
                                 headers={'Content-Type': 'application/json', 'Authorization': 'Bearer %s' % sessionid},
                                 data=json.dumps({
                                     'ParentId': sf_parent,
                                     'Name': sf_filename,
                                     'Body': body,
                                     'Description': sf_descr,
                                     'ContentType': 'application/pdf'
                                 })
        )
        return response.text

    def execute_soql_query(self, sfusername, sfpassword, sfsecurity_token, soqlquery, sandbox=True):
        # This function is used for fetching the soql query result from Salesforce, this needs above variable
        print('Following is Soql Query about to be executed %s' % soqlquery)
        sf = Salesforce(username=sfusername, password=sfpassword, security_token=sfsecurity_token, sandbox=True)
        soqlresult = sf.query(soqlquery)
        print(json.dumps(soqlresult))
        return soqlresult

    def execute_anon_apex(self, sfusername, sfpassword, sfsecurity_token, anon_apex, sandbox=True):
        print('Following apex is being executed: %s' % anon_apex)
        sf = Salesforce(username=sfusername, password=sfpassword, security_token=sfsecurity_token, sandbox=True)
        result = sf.restful('tooling/executeAnonymous',
                   {'anonymousBody': anon_apex})
        return result


    def getLeadEnrollmentSchool1(self, LeadId):
        ShoreLightID= self.sf.query("SELECT Shorelight_ID__c FROM Contact WHERE id ='" + ContactId + "'")
        print   ShoreLightID
        for x in ShoreLightID['records']:
            x['Shorelight_ID__c']
        ShoreLightID =  x['Shorelight_ID__c']
        return  ShoreLightID


