*** Settings ***
Library           AppiumLibrary    15
Library           BuiltIn
Library           Collections
Library           OperatingSystem
Library           Process
Library           String
Library           XML
Library           ReactLibrary
Library           FakerLibrary
Library           Library/Encode.py
Library           Library/SalesForceRestApi.py
Resource          Keywords/common-keywords.robot
Resource          Keywords/Faker-keywords.robot
Resource          Environments/${environment}.robot    #Resource    Environments/SP2QA.robot    ##########    Environment    (this needs to be point to file related to environment we need to test)
Resource          Variables/SPVariables.robot
Library           RequestsLibrary
Library           CSVLibrary
Library           ImapLibrary
Library           Library/Utilities.py
