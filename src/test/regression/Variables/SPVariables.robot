*** Variables ***
######################
#                 Common Variables
#####################
${BASEURL}			https://apply-qa.shorelight.ml/
${next_page_button}    xpath=//android.widget.Button[@resource-id='page-next-button']
${previous_page_button}    xpath=//android.widget.Button[@resource-id='page-previous-button']
${documents}    ${CURDIR}/../Documents
${imap_host}    imap.gmail.com
${email_port}    993
${email_user}    shorelightstudentportal@gmail.com
${email_password}    Shorel1ght1
${sender_email}    noreply@auburnglobal.org
${applications_email}    applications@auburnglobal.org
${email_timeout}    300


######################
#                 Personal Details
#####################
${input_first_name}    xpath=//android.widget.EditText[@resource-id='firstName']
${input_last_name}    xpath=//android.widget.EditText[@resource-id='lastName']
${input_email}    xpath=//android.widget.EditText[@resource-id='primaryEmail']
${auburn_major}    xpath=//android.widget.Spinner[@resource-id='majorAuburn']
${radio_start_term_spring}    //android.widget.RadioButton[@content-desc='Spring']
${2019_start_year}    //android.widget.RadioButton[@content-desc='2019']
${gender_male}    //android.widget.RadioButton[@content-desc='Male']
${gender_female}    //android.widget.RadioButton[@content-desc='Female']
${input_birthday_day}    xpath=//android.widget.Spinner[@resource-id='dateOfBirth-day']
${input_birthday_month}    xpath=//android.widget.Spinner[@resource-id='dateOfBirth-month']
${input_birthday_year}    xpath=//android.widget.Spinner[@resource-id='dateOfBirth-year']
${input_birth_city}    xpath=//android.widget.EditText[@resource-id='birthCity']
${input_birth_country}    xpath=//android.widget.Spinner[@resource-id='birthCountry']
${input_citizen_country}    xpath=//android.widget.Spinner[@resource-id='citizenshipCountry']


######################
#                 Contact Information
#####################


######################
#                 Education History
#####################


######################
#                 Documents & Transcripts
#####################


######################
#                 Agreements & Declarations
#####################


######################
#                 Validation Code Screen
#####################
# ${send_validation_code}    //*[text()='send validation code']
# ${send_new_validation_code}    //*[text()='send new code']

######################
#                 Validation Code Screen
#####################
${auburn_major}    majorAuburn-value
${auburn_start_term}    termAuburn-value
${auburn_start_year}    termYearAuburn-value
${sschool_country}    secondarySchool1Country-value
${ec_country}    ecAddressCountry-value

######################
#                 Review & Sign Screen
#####################
#Personal Information


#Contact Information
