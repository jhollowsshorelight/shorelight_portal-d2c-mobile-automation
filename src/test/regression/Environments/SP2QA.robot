*** Variables ***
${logintimeoutvalue}    30 sec
${BROWSER}        GC
#${BROWSER}        Edge
#${BROWSER}        ff

${desired_capabilities}    platform:LINUX,browserName:GC
#${desired_capabilities}    platform:WINDOWS,browserName:Edge
#${desired_capabilities}    platform:WINDOWS,browserName:ff
${browser_imp_wait}    2
${RETRY}          2
${DELAY}          0.1
${Wait_timeout}    90
${upload_timeout}    360
${BASEURL}        https://apply-qa.shorelight.ml/
${REMOTEGRIDURL}    http://54.88.247.199:4444/wd/hub

#China
#${REMOTEGRIDURL}    http://123.56.28.199:4444/wd/hub
${transactiontimeoutvalue}    120 sec
${TIMEOUT}        180
