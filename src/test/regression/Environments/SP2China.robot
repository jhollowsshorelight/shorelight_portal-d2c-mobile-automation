*** Variables ***
${logintimeoutvalue}    120 sec
${transactiontimeoutvalue}    120 sec
${BROWSER}        GC
${desired_capabilities}    platform:LINUX,browserName:GC
${browser_imp_wait}    5
${TIMEOUT}        360
${RETRY}          3
${DELAY}          0.5
${Wait_timeout}    360
${upload_timeout}    360
${BASEURL}        https://apply.shorelight.ml/
${REMOTEGRIDURL}    http://39.96.50.4:4444/wd/hub
