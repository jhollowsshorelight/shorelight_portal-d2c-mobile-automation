*** Keywords ***
Open Phone Browser To Undergraduate Page
    ${options}=    Create Dictionary    androidPackage=com.android.chrome
    ${caps}=    Create Dictionary    chromeOptions=${options}
    
    #Pixel 2 XL Emulator
    #${index}=    Open Application    http://localhost:4723/wd/hub    platformName=Android    deviceName=emulator-5554  appPackage=com.android.chrome   appActivity=com.google.android.apps.chrome.Main    newCommandTimeout=120

    #Pixel 3 XL Physical
    ${index}=    Open Application    http://localhost:4723/wd/hub    platformName=Android    deviceName=89PY09C6X  appPackage=com.android.chrome   appActivity=com.google.android.apps.chrome.Main    newCommandTimeout=120
    
    #LG G4 Physical
    #${index}=    Open Application    http://localhost:4723/wd/hub    platformName=Android    deviceName=VS9866c565d74  appPackage=com.android.chrome   appActivity=com.google.android.apps.chrome.Main    newCommandTimeout=120

    
    Set Suite Variable    ${app_index}    ${index}
    Wait Until Page Contains    Accept    ${Wait_timeout}
    Click Text    Accept
    Wait Until Page Contains    No Thanks    ${Wait_timeout}
    Click Text    No Thanks
    #${orig timeout} =    Set Appium Timeout    60 seconds
    #This times out more often than not and causes tests to fail.  Probably need a better solution to this.
    Run Keyword And Ignore Error    Go To Url    ${BASEURL}?school=Auburn&level=undergrad
    #Set Appium Timeout    ${orig timeout}	

Open Phone Browser To Graduate Page
    ${options}=    Create Dictionary    androidPackage=com.android.chrome
    ${caps}=    Create Dictionary    chromeOptions=${options}
    ${index}=    Open Application    http://localhost:4723/wd/hub    platformName=Android    deviceName=emulator-5554  appPackage=com.android.chrome   appActivity=com.google.android.apps.chrome.Main    newCommandTimeout=120
    Set Suite Variable    ${app_index}    ${index}
    Wait Until Page Contains    ACCEPT
    Click Text    ACCEPT
    Wait Until Page Contains    NO THANKS
    Click Text    NO THANKS
    
    Run Keyword And Ignore Error    Go To Url    ${BASEURL}?school=Auburn&level=graduate

Get Unique Email
    ${randomInt}    FakerLibrary.Random Int
    ${email}=    Set Variable    shorelightstudentportal+${FirstNameMaleF}_${randomInt}_${LastNameMaleF}@gmail.com
    [return]    ${email}

Personal Information
    [Arguments]     ${major}

    Wait Until Element Is Visible    ${input_first_name}    ${Wait_timeout}
    Input Text    ${input_first_name}    Test${FirstNameMaleF}
    Hide Keyboard
    Input Text    ${input_last_name}    Portal${LastNameMaleF}
    Hide Keyboard
    ${unique_email}=    Get Unique Email
    Set Suite Variable    ${email}    ${unique_email}
    Click Element    ${input_email}
    Sleep    1
    Input Text    ${input_email}    ${email}
    Hide Keyboard
    #Swipe to Element    ${auburn_major}
    Click Element    ${auburn_major}
    Wait Until Element Is Visible    //*[@text='${major}']    ${Wait_timeout}
    Click Element    //*[@text='${major}']
    #Switch Application    ${app_index}
    #Click Element    //*[@content-desc='Major / Area of Interest']
    #Swipe to Element    ${radio_start_term_spring}
    Swipe    200    1000    200    600    800
    #Swipe to Text    Spring
    Click Text    Spring
    Swipe    200    1000    200    600    800
    #Swipe to Text    2019
    Click Text    2019
    Swipe    200    1000    200    600    800
    #Swipe to Text    Male
    Click Text    Male

    Swipe    200    1000    200    600    800
    Click Element    ${input_birthday_day}
    Wait Until Element Is Visible    //*[@text='1']    ${Wait_timeout}
    Click Element    //*[@text='1']

    Wait Until Page Contains Element    ${input_birthday_month}    ${Wait_timeout}
    Click Element    ${input_birthday_month}
    Wait Until Element Is Visible    //*[@text='January']    ${Wait_timeout}
    Click Element    //*[@text='January']    
    
    Wait Until Page Contains Element    ${input_birthday_year}    ${Wait_timeout}
    Click Element    ${input_birthday_year}
    Wait Until Element Is Visible    //*[@text='2005']    ${Wait_timeout}
    Click Element    //*[@text='2005']

    Swipe    200    1000    200    600    800
    Input Text    ${input_birth_city}    ${CityF}
    Hide Keyboard
    #Sleep    30
    #${source} =    Get Source
    #Log To Console    ${source}
    Click Element    ${input_birth_country}
    Wait Until Element Is Visible    //*[@text='Albania']    ${Wait_timeout}
    Click Element    //*[@text='Albania']
    
    Click Element    ${input_citizen_country}
    Wait Until Element Is Visible    //*[@text='Albania']    ${Wait_timeout}
    Click Element    //*[@text='Albania']
    Swipe    200    1000    200    600    800

    Click Text    My family will be funding my education.
    Wait Until Page Contains    This step is complete!    ${Wait_timeout}
    Wait Until Page Contains    Please continue to step 2    ${Wait_timeout}

    #${source} =    Get Source
    #Log To Console    ${source}
    #Wait Until Element Is Visible    //*[@text='Albania']    ${Wait_timeout}

    Take Screenshot With Timestamp
    Click Element    ${next_page_button}

Contact Information
    Log To Console    TODO

    # Click Element    ${next_page_button}

Education History
    Log To Console    TODO
    # Click Element    ${next_page_button}

Graduate Education History
    Log To Console    TODO
    # Click Element    ${next_page_button}    

Upload Documents
    Log To Console    TODO
    
    #Use this to push files to phone for later use for uploading documents in the app.
    # ${contents} =    Get Binary File    ${documents}/Financial.jpg
    # Push File    /mnt/sdcard/Pictures/Financial.jpg    ${contents}    True
    #Add Code to select file in Android to upload.  Not that this will most likely be specific to OS version.
    
    # Click Element    ${next_page_button}

Agreements And Declarations
    Log To Console    TODO
    # Click Element    ${review_and_sign}

Take Screenshot With Timestamp
   ${time} =    Get Time
   ${time}=    Replace String    ${time}    :    .    -1
   ${test}=    Get Variable Value    ${TEST_NAME}    ${EMPTY}
   #${file_name}=    Set Variable    ${SUITE_NAME}_${test}_${time}
   ${file_name}=    Set Variable    ${test}_${time}
   ${file_name}=    Remove String    ${file_name}    \    /    :    *
   ...    ?    "    <    >    |    .    -    ${SPACE}
   Capture Page Screenshot    ${OUTPUTDIR}//screenshot-${file_name}.png

Close Browser And Take Screenshot If Required
    Run Keyword If Test Failed    Take Page Failure Screenshots
    Quit Application

Take Page Failure Screenshots
    [Documentation]    This keyword will take screenshots to help diagnose data entry issues during test runs
    Take Screenshot With Timestamp
    Press Keys    None    PAGE_UP
    Press Keys    None    PAGE_UP
    Press Keys    None    PAGE_UP
    Take Screenshot With Timestamp
    Press Keys    None    PAGE_DOWN
    Sleep    500ms
    Take Screenshot With Timestamp
    Press Keys    None    PAGE_DOWN
    Sleep    500ms
    Take Screenshot With Timestamp
    Press Keys    None    PAGE_DOWN
    Sleep    500ms
    Take Screenshot With Timestamp    
    #Close Browser
    #Fatal Error

Swipe to Element
    [documentation]    Keyword will scroll the screen until the ${element} that is found on the screen and then return.
    [Arguments]    ${element}    ${count}=10
    : FOR    ${index}    IN RANGE    0    ${count}
    \    ${isVisible} =  Run Keyword And Return Status   Element Should Be Visible    ${element}
    \    Run Keyword If    ${isVisible}     Exit For Loop
    \    Swipe    200    1000    200    200    800
    #\    Swipe By Percent  30    40    30    10    1000    
    \    Log To Console    Not found.  Looping...
    \    ${index}    Set Variable    ${index}+1
    Log To Console    Visible:  ${isVisible}

Swipe to Text
    [documentation]    Keyword will scroll the screen until the ${element} that is found on the screen and then return.
    [Arguments]    ${element}    ${count}=10
    : FOR    ${index}    IN RANGE    0    ${count}
    \    ${isVisible} =  Run Keyword And Return Status   Text Should Be Visible    ${element}
    \    Run Keyword If    ${isVisible}     Exit For Loop
    \    Swipe    200    1000    200    600    800
    #\    Swipe By Percent  30    40    30    10    1000    
    \    Log To Console    Not found.  Looping...
    \    ${index}    Set Variable    ${index}+1
