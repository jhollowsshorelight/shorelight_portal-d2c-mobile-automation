*** Keywords ***
FakerLibrary FirstName
    ${FirstName}=    FakerLibrary.First Name
    Log    ${FirstName}
    Set Suite Variable    ${FirstNameF}    ${FirstName}
    #[Return]    ${FirstName}

FakerLibrary First Name Male
    ${FirstNameMale}=    FakerLibrary.First Name Male
    Log    ${FirstNameMale}
    Set Suite Variable    ${FirstNameMaleF}    ${FirstNameMale}
    #[Return]    ${FirstNameMale}

FakerLibrary Last Name Male
    ${LastNameMale}=    FakerLibrary.Last Name Male
    Log    ${LastNameMale}
    Set Suite Variable    ${LastNameMaleF}    ${LastNameMale}
    #[Return]    ${LastNameMale}

FakerLibrary Prefix Male
    ${PrefixMale}=    FakerLibrary.Prefix Male
    Log    ${PrefixMale}
    Set Suite Variable    ${PrefixMaleF}    ${PrefixMale}
    #[Return]    ${PrefixMale}

FakerLibrary LastName
    ${LastName}=    FakerLibrary.Last Name
    Log    ${LastName}
    Set Suite Variable    ${LastNameF}    ${LastName}

FakerLibrary StreetAddress
    ${StreetAddress}=    FakerLibrary.Street Address
    Log    ${StreetAddress}
    Set Suite Variable    ${StreetAddressF}    ${StreetAddress}

FakerLibrary USStreetAddress
    ${USStreetAddress}=    FakerLibrary.Street Address
    Log    ${USStreetAddress}
    Set Suite Variable    ${USStreetAddressF}    ${USStreetAddress}

FakerLibrary StreetSuffix
    ${StreetSuffix}=    FakerLibrary.Street Suffix
    Log    ${StreetSuffix}
    Set Suite Variable    ${StreetSuffixF}    ${StreetSuffix}    

FakerLibrary USStreetSuffix
    ${USStreetSuffix}=    FakerLibrary.Street Suffix
    Log    ${USStreetSuffix}
    Set Suite Variable    ${USStreetSuffixF}    ${USStreetSuffix}    

FakerLibrary City
    ${City}=    FakerLibrary.City
    Log    ${City}
    Set Suite Variable    ${CityF}    ${City}

FakerLibrary State
    ${State}=    FakerLibrary.State
    Log    ${State}
    Set Suite Variable    ${StateF}    ${State}

FakerLibrary PostalCode
    ${PostalCode}=    FakerLibrary.Postal Code
    Log    ${PostalCode}
    Set Suite Variable    ${PostalCodeF}    ${PostalCode}

FakerLibrary Email
    ${Email}=    FakerLibrary.Email
    Log    ${Email}
    Set Suite Variable    ${EmailF}    ${Email}

FakerLibrary Country
    ${Country}=    FakerLibrary.Country
    Log    ${Country}
    Set Suite Variable    ${CountryF}    ${Country}

FakerLibrary Prefix
    ${Prefix}=    FakerLibrary.Prefix
    Log    ${Prefix}
    Set Suite Variable    ${PrefixF}    ${Prefix}

FakerLibrary Msisdn
    ${Msisdn}=    FakerLibrary.Msisdn
    Log    ${Msisdn}
    Set Suite Variable    ${MsisdnF}    ${Msisdn}

FakerLibrary CountryCode
    ${country_code}=    FakerLibrary.Country Code
    Log    ${country_code}
    Set Suite Variable    ${CountryCodeF}    ${country_code}

FakerLibrary PhoneNumber
    ${phone_number}=    FakerLibrary.Phone Number
    Log    ${phone_number}
    Set Suite Variable    ${PhoneNumberF}    ${phone_number}

Generate Faker Data
    FakerLibrary FirstName
    FakerLibrary LastName
    FakerLibrary StreetAddress
    FakerLibrary City
    FakerLibrary State
    FakerLibrary PostalCode
    FakerLibrary Email
    FakerLibrary Country
    FakerLibrary Prefix
    FakerLibrary Msisdn
    FakerLibrary First Name Male
    FakerLibrary Last Name Male
    FakerLibrary Prefix Male
    FakerLibrary Country Code
    FakerLibrary Phone Number
    FakerLibrary USStreetAddress
    FakerLibrary StreetSuffix
    FakerLibrary USStreetSuffix

